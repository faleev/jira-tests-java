# Test Suite for Jira #

This project aims to demonstrate basic approach of web automation using Jira CRUD operation as example.

### Under the hood ###

* Framework based on TestNG and Maven
* ReportNG is used for reporting (extended to make screenshots on test fails)
* Page Object pattern used for describing of page's elements and interaction.
* Some project properties (e.g. user, password) are placed into resources.
* Testing in the different browsers (Chrome 37.0.2062.124, Firefox 32.0.3) is also illustrated.

### How to run tests and review results ###

* Make sure you have Java 1.7 and Maven 3 installed.
* Open command line.
* Go to the project directory.
* Run the command: ```mvn clean install```
* Wait until all tests will be executed.
* Observe results here: `${PROJECT_LOCATION}/target/reports/html/index.html`
* In case of any issues you will see screenshots there.
* Send feedback.

### Contribution guidelines ###

* Welcome to contribute.
* No other guidelines for now.